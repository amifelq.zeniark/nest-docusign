import { Type } from 'class-transformer';
import { IsNotEmpty, IsOptional, IsString, Validate } from 'class-validator';
import { RecipientsArray } from './create-template-recipents';

export class CreateTemplateDTO {
  @IsNotEmpty()
  @Validate(RecipientsArray)
  signers: string;

  @IsNotEmpty()
  @Validate(RecipientsArray)
  ccs: string;

  @IsNotEmpty()
  @IsString()
  subject: string;

  @IsNotEmpty()
  @IsString()
  tempDescription: string;

  @IsNotEmpty()
  @IsString()
  tempName: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  docName: string;
}
