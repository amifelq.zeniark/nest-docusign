import { Type } from 'class-transformer';
import {
  IsAlphanumeric,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

class Recipient {
  @IsOptional()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsOptional()
  @IsNotEmpty()
  @IsAlphanumeric()
  name: string;
}
export class UpdateTemplateDTO {
  @IsOptional()
  @IsNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => Recipient)
  signers: Recipient[];

  @IsOptional()
  @IsNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => Recipient)
  ccs: Recipient[];

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  subject: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  tempDescription: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  tempName: string;

  // @IsNotEmpty()
  // @IsString()
  // documentId: string;
}
