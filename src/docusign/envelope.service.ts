import { Injectable } from "@nestjs/common";
import { EnvelopeDefinition, TemplateRole, SignHere } from 'docusign-esign';

@Injectable()
export class EnvelopeService {
  createEnvelope(options) {
    const envelope = new EnvelopeDefinition();
    envelope.templateId = options.templateId;

    if (options.signer && options.cc) {
      // create template role for signer
      const signer1 = new TemplateRole();
      signer1.roleName = 'signer';
      signer1.name = options.signer.name || '';
      signer1.email = options.signer.email;

      // via setters - lib/esignature/examples/createTemplate
      // create template role for cc
      const cc1 = new TemplateRole();
      cc1.roleName = 'cc';
      cc1.name = options.cc.name || '';
      cc1.email = options.cc.email;

      envelope.templateRoles = [signer1, cc1];
    }

    // staus created
    envelope.status = 'sent';
    return envelope;
  }

  updateEnvelope(options = null) {
    const envelope = new EnvelopeDefinition();

    // expand options to other envelope definition properties if needed
    // for now, update status to sent
    envelope.status = 'sent';
    return envelope;
  }

  purgeEnvelope() {
    const envelope = new EnvelopeDefinition();

    envelope.purgeState = 'documents_queued';
    return envelope;
  }
}
