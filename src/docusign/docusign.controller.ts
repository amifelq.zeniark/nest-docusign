import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Req,
  Res,
  HttpStatus,
  Query,
  HttpException,
  Put,
  CACHE_MANAGER,
  Inject,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { readFileSync, writeFile } from 'fs';
import { Cache } from 'cache-manager';
import { DocusignService } from './docusign.service';
import { CreateTemplateDTO } from './dto/create-template.dto';
import { SendEnvelopeDto } from './dto/send-envelope.dto';
import { UpdateTemplateDTO } from './dto/update-template.dto';
import { AuthRequest } from 'src/interfaces/request.interface';
import { FileInterceptor } from '@nestjs/platform-express';
import { documentFileFilter, uploadOptions } from 'src/helpers/upload.utils';

@Controller('ds')
export class DocusignController {
  constructor(
    private readonly docusignService: DocusignService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {}

  @Get('url')
  getAuthURL(@Res() res: Response) {
    return res.status(HttpStatus.OK).json({
      message: 'Click URL to consent Docusign',
      url: `${process.env.DS_AUTH_SERVER}/oauth/auth?response_type=code&scope=signature%20impersonation&client_id=${process.env.DS_CLIENT_ID}&redirect_uri=${process.env.DS_APP_URL}/ds/callback`,
    });
  }

  @Post('requestToken')
  async login(@Req() req: Request, @Res() res: Response) {
    // TODO: move to middleware, verify access token and regenerate if needed
    const result = await this.docusignService.login();

    if (result.error !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: result.error,
      });
    }
    await this.cacheManager.set('token', result.access_token, {
      ttl: process.env.TOKEN_EXP || result.expires,
    });
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Success',
      data: result,
    });
  }

  @Get('templates/:id')
  async getTemplate(
    @Req() req: AuthRequest,
    @Res() res: Response,
    @Param('id') templateId: string,
  ) {
    const response = await this.docusignService.getTemplate(templateId);

    if (response.error !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: response.error,
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Template successfully retrieved',
      data: response,
    });
  }

  @Get('templates')
  async getAllTemplates(@Req() req: AuthRequest, @Res() res: Response) {
    const response = await this.docusignService.getAllTemplates();

    if (response.error !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: response.error,
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Templates list successfully retrieved',
      data: response,
    });
  }

  @Post('templates/create')
  async postTemplate(@Req() req: AuthRequest, @Res() res: Response) {
    // demo file
    const file = readFileSync('src/files/Template2 - StaticForm.pdf');

    if (!file) throw new HttpException('File not found', HttpStatus.NOT_FOUND);

    const fileBase64 = Buffer.from(file).toString('base64');

    const response = await this.docusignService.createTemplate(fileBase64);

    if (response.error !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: response.error,
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Template successfully created',
      data: response,
    });
  }

  @UseInterceptors(
    FileInterceptor('file', uploadOptions('./src/files', documentFileFilter)),
  )
  @Post('templates/uploadAndCreate')
  async postUploadCreateTemplate(
    @Req() req: AuthRequest,
    @Res() res: Response,
    @UploadedFile() file,
    @Body() body: CreateTemplateDTO,
  ) {
    if (!file)
      throw new HttpException('File is required', HttpStatus.BAD_REQUEST);

    const fileStream = readFileSync(file.path);

    const fileBase64 = Buffer.from(fileStream).toString('base64');

    const response = await this.docusignService.createTemplateWithTags({
      file: fileBase64,
      ...body,
    });

    if (response.error !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: response.error,
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Template successfully created',
      data: response,
    });
  }

  @Put('templates/:id/update')
  async putTemplate(
    @Req() req: Request,
    @Res() res: Response,
    @Param('id') templateId: string,
    @Body() body: UpdateTemplateDTO,
  ) {
    let response = await this.docusignService.updateTemplateRecipients(
      templateId,
      body,
    );

    if (response.error !== undefined || response.errorDetails !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message:
          response.error ||
          response.errorDetails?.message ||
          'Template not updated',
      });
    }

    response = await this.docusignService.updateTemplate(templateId, body);

    if (response.error !== undefined || response.errorDetails !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message:
          response.error ||
          response.errorDetails?.message ||
          'Template not updated',
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Template successfully updated',
      data: response,
    });
  }

  // expand to batch delete - add request body
  @Delete('templates/:id/delete')
  async deleteTemplate(
    @Req() req: Request,
    @Res() res: Response,
    @Param('id') templateId: string,
  ) {
    const response = await this.docusignService.delete(templateId);

    if (response.error !== undefined || response.errorDetails !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message:
          response.error ||
          response.errorDetails?.message ||
          'Template not deleted',
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Template successfully deleted',
      data: response,
    });
  }

  @Get('templates/:id/download')
  async downloadTemplate(
    @Req() req: AuthRequest,
    @Res({ passthrough: true }) res: Response,
    @Param('id') templateId: string,
    @Body('documentId') documentId: string,
  ) {
    console.log(documentId);
    const response = await this.docusignService.downloadTemplateDocs(
      templateId,
      'combined',
    );

    if (response.error !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: response.error,
      });
    }

    const randomName = Array(10)
      .fill(null)
      .map(() => Math.round(Math.random() * 16).toString(16))
      .join('');

    writeFile(
      `./src/files/${randomName}.pdf`,
      response,
      { encoding: 'binary' },
      (err) => {
        if (err) throw err;
        console.log('Saved!');
      },
    );

    // return response;
    res.header('Content-Type', 'application/pdf');
    return res.status(HttpStatus.OK).end(response);
  }

  @Post('envelopes/create')
  async postEnvelope(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: SendEnvelopeDto,
  ) {
    // check template if exisiting
    let response = await this.docusignService.getTemplate(body.templateId);

    if (response.error !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: response.error,
      });
    }

    // no error, send envelope - draft
    response = await this.docusignService.sendEnvelope(body);

    if (response.error !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: response.error,
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Envelope successfully created',
      data: response,
    });
  }

  @Get('envelopes')
  async getAllEnvelopes(@Req() req: Request, @Res() res: Response) {
    const result = await this.docusignService.getAllEnvelopes();

    if (result['error']) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: result['error'],
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Envelopes list successfully retrieved',
      data: {
        count: result.length,
        envelopes: result,
      },
    });
  }

  @Get('envelopes/:id')
  async getEnvelope(
    @Req() req: Request,
    @Res() res: Response,
    @Param('id') envelopeId: string,
  ) {
    const result = await this.docusignService.getEnvelope(envelopeId);

    if (result.error) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: result.error,
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Envelope details successfully retrieved',
      data: result,
    });
  }

  @Put('envelopes/:id/resend')
  async putResendEnvelope(
    @Req() req: Request,
    @Res() res: Response,
    @Param('id') envelopeId: string,
  ) {
    // check template if exisiting
    const result = await this.docusignService.getEnvelope(envelopeId);

    // cannot fetch envelope
    if (result.error) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: result.error,
      });
    }

    // check if envelope status is completed
    // TODO: check other envelope statuses
    if (result.status === 'completed') {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'Envelope already completed',
      });
    }

    // update status to sent, send envelope
    const response = await this.docusignService.updateEnvelope({
      envelopeId,
    });

    if (response.error !== undefined || response.errorDetails !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message:
          response.error ||
          response.errorDetails?.message ||
          'Envelope not sent',
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Envelope successfully sent',
      data: response,
    });
  }

  // Update envelope to sent, can be expanded for additional or removal of recipients / signers from body
  @Put('envelopes/:id/update')
  async putEnvelope(
    @Req() req: Request,
    @Res() res: Response,
    @Param('id') envelopeId: string,
    @Body() body: SendEnvelopeDto,
  ) {
    // check template if exisiting
    const result = await this.docusignService.getEnvelope(envelopeId);

    // cannot fetch envelope
    if (result.error) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: result.error,
      });
    }

    // check if envelope status is completed
    // TODO: check other envelope statuses
    if (result.status === 'completed') {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'Envelope already completed',
      });
    }

    // update status to sent, send envelope
    const response = await this.docusignService.updateEnvelope({
      envelopeId,
      templateId: body.templateId,
    });

    if (response.error !== undefined || response.errorDetails !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message:
          response.error ||
          response.errorDetails?.message ||
          'Envelope not sent',
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Envelope successfully sent',
      data: response,
    });
  }

  // TODO: expand to batch delete - add request body
  /**
   * Updates envelope status to voided - sends notification to ccs
   */
  @Delete('envelopes/:id/delete')
  async deleteEnvelope(
    @Req() req: Request,
    @Res() res: Response,
    @Param('id') envelopeId: string,
  ) {
    const response = await this.docusignService.delete(envelopeId);

    if (response.error !== undefined || response.errorDetails !== undefined) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message:
          response.error ||
          response.errorDetails?.message ||
          'Envelope not deleted',
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Envelope successfully deleted',
      data: response,
    });
  }

  @Delete('envelopes/:id')
  async purgeEnvelope(
    @Req() req: Request,
    @Res() res: Response,
    @Param('id') envelopeId: string,
  ) {
    const result = await this.docusignService.getEnvelope(envelopeId);

    if (result.error) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: result.error,
      });
    }

    // queue document for purging, permanently deleted after a time
    const response = await this.docusignService.deleteEnvelope(envelopeId);

    if (response.error) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: response.status || HttpStatus.BAD_REQUEST,
        message: response.error,
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Envelope successfully purged',
      data: response,
    });
  }

  @Get('callback')
  callback(@Query() query) {
    return query;
  }

  @Get('user')
  async getUser(@Req() req: AuthRequest, @Res() res: Response) {
    const result = await this.docusignService.getUserLoggedIn();

    if (result.error) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: result.status || HttpStatus.BAD_REQUEST,
        message: result.error,
      });
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'User details successfully retrieved',
      data: result,
    });
  }
}
