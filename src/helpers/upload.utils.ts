import { existsSync, mkdir } from 'fs';
import { diskStorage } from 'multer';
import { extname } from 'path';

export const PROFILE_DIRECTORY = './uploads/profile';
export const uploadOptions = (destination, fileFilter) => ({
  storage: diskStorage({
    destination,
    filename: editFileName,
  }),
  fileFilter,
});

export const createDirectory = (destination) => {
  try {
    if (!existsSync(destination))
      mkdir(destination, { mode: '0777' }, (err, path) => {
        if (err) throw new Error(err);
      });
    return destination;
  } catch (error) {
    console.warn(error);
    return null;
  }
};

export const imageFileFilter = (req, file, callback) => {
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return callback(new Error('Only image files are allowed!'), false);
  }
  callback(null, true);
};

export const documentFileFilter = (req, file, callback) => {
  if (!file.originalname.match(/\.(pdf)$/)) {
    return callback(new Error('Only pdf file allowed!'), false);
  }
  callback(null, true);
};

export const editFileName = (req, file, callback) => {
  // const name = file.originalname.split('.')[0];
  const fileExtName = extname(file.originalname);
  const randomName = Array(4)
    .fill(null)
    .map(() => Math.round(Math.random() * 16).toString(16))
    .join('');
  callback(null, `${randomName}${fileExtName}`);
};
