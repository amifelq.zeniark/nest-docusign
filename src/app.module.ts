import {
  CacheModule,
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DocusignModule } from './docusign/docusign.module';
import { AuthMiddleware } from './docusign/auth.middleware';
import * as dotenv from 'dotenv';
import { DocusignController } from './docusign/docusign.controller';
dotenv.config();

@Module({
  imports: [DocusignModule, CacheModule.register({ isGlobal: true })],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude(
        { path: 'ds/url', method: RequestMethod.GET },
        { path: 'ds/requestToken', method: RequestMethod.POST },
        { path: 'ds/callback', method: RequestMethod.GET },
      )
      .forRoutes(DocusignController);
  }
}
